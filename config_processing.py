import os

from utils.network_config import NetworkDevice


def process_push_config(config_template_filename, routers, bandwidth=None):
    base_directory = os.path.dirname(os.path.abspath(__file__))
    templates_directory = os.path.join(base_directory, 'templates')
    config_template_path = os.path.join(templates_directory, config_template_filename)
    var_directory = os.path.join(base_directory, 'var')
    print(bandwidth)
    results = {}
    for router_name, router_data in routers.items():
        config_data_filename = f'{router_name.lower()}_data.yaml'
        config_data_path = os.path.join(var_directory, config_data_filename)
        result = push_config_to_router(router_data, config_template_path, config_data_path, bandwidth)
        results[router_name] = result

    return results


def push_config_to_router(router_data, config_template_path, config_data_path, bandwidth=None):
    device = NetworkDevice()
    print(router_data)
    alias = device.connect(
        router_data['host'],
        router_data['driverName'],
        router_data['username'],
        router_data['password'],
        transport=router_data.get('transport'),
        port=router_data['port']
    )
    print(bandwidth)
    result = device.push_config_from_template(config_template_path, config_data_path, bandwidth)
    device.disconnect()

    return result
