import requests
from dotenv import load_dotenv
from flask import Flask, request, jsonify
import os
from utils.connectivity_tests import test_vpn_l3_connectivity_with_input, test_internet_connectivity_with_input
from config_processing import process_push_config

load_dotenv()
app = Flask(__name__)
spring_boot_api_url = os.environ["SPRING_BOOT_API_URL"]


@app.route('/push', methods=['POST'])
def push_config_device():
    try:
        data = request.get_json()
        response = requests.post(f"{spring_boot_api_url}/getRouters", json=data)
        routers = response.json()
        results = process_push_config('l3vpn_config.j2', routers)

        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


@app.route('/pushQos', methods=['POST'])
def push_config_device_Qos(data=None):
    try:
        if not data:
            data = request.get_json()
        response = requests.post(f"{spring_boot_api_url}/getRouters", json=data)
        routers = response.json()
        bandwidth = data['bandwidth'] * 1000
        print(bandwidth)
        results = process_push_config('Qos_config.j2', routers, bandwidth)

        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


@app.route('/pushNetwork', methods=['POST'])
def push_config_device_network():
    try:
        data = request.get_json()
        print(data)
        response = requests.post(f"{spring_boot_api_url}/getRouterCE", json=data)
        routers = response.json()
        results = process_push_config('internet_via_mpls.j2', routers)
        qos_results = push_config_device_Qos(data)
        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


@app.route('/test', methods=['POST'])
def test_push_config_device():
    data = request.get_json()
    api_url = f"{spring_boot_api_url}/robot-test"
    return test_vpn_l3_connectivity_with_input(api_url, data)


@app.route('/testNetwork', methods=['POST'])
def testNetwork_push_config_device():
    data = request.get_json()
    api_url = f"{spring_boot_api_url}/robot-test"
    return test_internet_connectivity_with_input(api_url, data)


@app.route('/rollback', methods=['POST'])
def push_config_device_rollBack():
    try:
        data = request.get_json()
        response = requests.post(f"{spring_boot_api_url}/getRouters", json=data)
        routers = response.json()
        results = process_push_config('L3vpn_config_rollback.j2', routers)

        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


def push_config_device_rollBackDebit(data=None):
    try:
        response = requests.post(f"{spring_boot_api_url}/getRouters", json=data)
        routers = response.json()
        results = process_push_config('deleteBandwidth.j2', routers)

        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


@app.route('/rollbackNetwork', methods=['POST'])
def push_config_device_rollBackNetwork():
    try:
        data = request.get_json()
        response = requests.post(f"{spring_boot_api_url}/getRouterCE", json=data)
        routers = response.json()
        results = process_push_config('internet_via_mpls_rollback.j2', routers)
        debit_results = push_config_device_rollBackDebit(data)
        return jsonify({"status": "success", "devices": results})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)})


if __name__ == '__main__':
    app.run()
