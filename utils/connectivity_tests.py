from utils.api_utils import get_router_data_from_api
import subprocess
import os
import sys

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
utils_path = os.path.join(project_root, 'utils')


def test_vpn_l3_connectivity_with_input(api_url, data):
    router_data = get_router_data_from_api(api_url, data)
    connectivity_results = {}

    first_device = router_data[0]
    second_device = router_data[1]

    source_ip = first_device["ipAddressInterface"]
    destination_ip = second_device["ipAddressInterface"]
    port = first_device["port"]
    transport = first_device["transport"]
    driver_name = first_device["driver"]
    host = first_device["ip"]

    robot_file_path = os.path.join(os.getcwd(), "utils", "test_connectivity.robot")

    command = f"robot --loglevel DEBUG -v SOURCE_IP:{source_ip} -v DESTINATION_IP:{destination_ip} -v PORT:{port} -v " \
              f"TRANSPORT:{transport} -v DRIVER_NAME:{driver_name} -v HOST:{host} {robot_file_path}"
    env = os.environ.copy()
    env["PATH"] = f"{os.path.dirname(sys.executable)}{os.pathsep}{env['PATH']}"
    env["PYTHONPATH"] = f"{utils_path};{env.get('PYTHONPATH', '')}"
    result = subprocess.run(command, shell=True, capture_output=True, text=True, env=env)

    stdout = result.stdout
    stderr = result.stderr
    return_code = result.returncode

    connectivity_results = {
        "stdout": stdout,
        "stderr": stderr,
        "return_code": return_code
    }
    return connectivity_results


def test_internet_connectivity_with_input(api_url, data):
    router_data = get_router_data_from_api(api_url, data)
    connectivity_results = {}

    first_device = router_data[0]
    google_dns = "8.8.8.8"

    source_ip = first_device["ipAddressInterface"]
    destination_ip = google_dns
    port = first_device["port"]
    transport = first_device["transport"]
    driver_name = first_device["driver"]
    host = first_device["ip"]

    robot_file_path = os.path.join(os.getcwd(), "utils", "test_connectivity.robot")

    command = f"robot --loglevel DEBUG -v SOURCE_IP:{source_ip} -v DESTINATION_IP:{destination_ip} -v PORT:{port} -v " \
              f"TRANSPORT:{transport} -v DRIVER_NAME:{driver_name} -v HOST:{host} {robot_file_path}"
    env = os.environ.copy()
    env["PATH"] = f"{os.path.dirname(sys.executable)}{os.pathsep}{env['PATH']}"
    env["PYTHONPATH"] = f"{utils_path};{env.get('PYTHONPATH', '')}"
    result = subprocess.run(command, shell=True, capture_output=True, text=True, env=env)

    stdout = result.stdout
    stderr = result.stderr
    return_code = result.returncode

    connectivity_results = {
        "stdout": stdout,
        "stderr": stderr,
        "return_code": return_code
    }
    return connectivity_results
