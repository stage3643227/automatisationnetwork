from robot.api.deco import keyword

from network_config import NetworkDevice


class NetworkDeviceLibrary:

    def __init__(self):
        self.network_device = NetworkDevice()

    @keyword("Connect")
    def connect(self, host, driver_name, username, password, transport='ssh', port=None, alias=None, exclusive=False):
        return self.network_device.connect(host, driver_name, username, password, transport, port, alias, exclusive)

    @keyword("Disconnect")
    def disconnect(self):
        self.network_device.disconnect()

    @keyword("Execute Ping Command")
    def execute_ping_command(self, destination_ip, source_ip=None):
        return self.network_device.execute_ping_command(destination_ip, source_ip)
