import os
from napalm.ios.ios import HOUR_SECONDS, DAY_SECONDS, WEEK_SECONDS, YEAR_SECONDS  # noqa
from napalm.ios.ios import IPV6_ADDR_REGEX_2, IPV6_ADDR_REGEX_3, IPV6_ADDR_REGEX  # noqa
from napalm.ios.ios import IP_ADDR_REGEX, IPV4_ADDR_REGEX, IPV6_ADDR_REGEX_1  # noqa

from napalm import get_network_driver, logger
import yaml
from jinja2 import Environment, FileSystemLoader


class NetworkDevice:
    def __init__(self):
        self.device = None
        self.driver = None
        self.password = None
        self.username = None
        self.driver_name = None
        self.host = None

    def connect(self, host, driver_name, username, password, transport='ssh', port=None, alias=None, exclusive=False):
        args = {}
        self.host = host
        self.driver_name = driver_name
        self.username = username
        self.password = password
        self.driver = get_network_driver(self.driver_name)

        args['global_delay_factor'] = 2
        if transport == 'telnet':
            args['transport'] = 'telnet'
            args['port'] = port
        else:
            args['transport'] = 'ssh'
            args['port'] = 22
        if exclusive:
            args['config_lock'] = True

        self.device = self.driver(hostname=host,
                                  username=username,
                                  password=password,
                                  optional_args=args)
        self.device.open()
        return "connect"

    def disconnect(self):
        self.device.close()

    def push_config_from_template(self, config_template_path, config_data_path, bandwidth=None):
        template_directory = os.path.dirname(config_template_path)
        template_filename = os.path.basename(config_template_path)

        j2_env = Environment(loader=FileSystemLoader(template_directory), trim_blocks=True)

        config_template = j2_env.get_template(template_filename)

        with open(config_data_path) as yaml_file:
            config_data = yaml.load(yaml_file, yaml.Loader)
        print("Loaded config data:\n", config_data)
        print(bandwidth)
        if bandwidth is not None:
            config_data["interface"]["bandwidth"] = bandwidth

        rendered_config_temp = config_template.render(**config_data)
        print("Rendered config:\n", rendered_config_temp)
        logger.info(f"Rendered config: {rendered_config_temp}")
        self.device.load_merge_candidate(config=rendered_config_temp)
        self.device.commit_config()

        return "OK"

    def execute_ping_command(self, destination_ip, source_ip=None):
        if self.device is None:
            raise RuntimeError("Device not connected")

        if source_ip:
            ping_output = self.device.ping(destination=destination_ip, source=source_ip)
        else:
            ping_output = self.device.ping(destination=destination_ip)

        return ping_output
