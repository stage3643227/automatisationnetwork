*** Settings ***
Library           utils.NetworkDeviceLibrary
Library           utils.network_config
Library           Collections
*** Variables ***
${HOST}
${DRIVER_NAME}
${USERNAME}
${PASSWORD}
${transport}
${port}
${source_ip}
${destination_ip}


*** Test Cases ***
Ping Test
    Connect  ${HOST}  ${DRIVER_NAME}  ${USERNAME}  ${PASSWORD}  ${transport}  ${port}
    ${ping_output}=  Execute Ping Command  ${destination_ip}  ${source_ip}
    Log  ${ping_output}
    ${json_contains_success}=  Run Keyword And Return Status  Collections.Dictionary Should Contain Key  ${ping_output}  success
    Log  JSON contains success: ${json_contains_success}
    Should Be True  ${json_contains_success}
    Disconnect
