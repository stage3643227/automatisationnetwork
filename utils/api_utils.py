import requests


def get_router_data_from_api(api_url, data):
    response = requests.post(api_url, json=data)
    router_data = response.json()["results"]

    filtered_data = []
    for device in router_data:
        device_info = {
            "device": device["deviceName"],
            "ip": device["ipAddress"],
            "port": device["port"],
            "transport": device["transport"],
            "driver": device["driver"],
        }
        for net_interface in device["netInterfaces"]:
            if net_interface["nameInterface"] == "Ethernet0/1":
                device_info["nameInterface"] = net_interface["nameInterface"]
                device_info["ipAddressInterface"] = net_interface["ipAddressInterface"]
                break
        filtered_data.append(device_info)
    return filtered_data
