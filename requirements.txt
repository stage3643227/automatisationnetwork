requests~=2.28.2
PyYAML~=6.0
Jinja2~=3.1.2
napalm~=4.0.0
robotframework~=6.0.2
Flask~=2.2.3
setuptools~=65.5.1
python-dotenv~=1.0.0